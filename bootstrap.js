/*
 * Bootstrap.js version @VERSION
 * Copyright (c) 2012 AUTHORS.txt
 * Licensed under the same terms as Bootstrap.js
 */
steal('jquery').then(
		'./bootstrap/js/bootstrap.js'
		).then(
	// style files
	'./bootstrap/css/bootstrap.css'
	// Responsive:
	,'./bootstrap/css/bootstrap-responsive.css'
);

