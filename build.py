

import os
basedir = os.path.join('core', 'js')
#First inventory the files in the core/js directory
for fname in  os.listdir(basedir):
    if fname.endswith('.js'):
        fullname = os.path.join(basedir, fname)
        dirname = fname[:-3].split('-')[1]
        if not os.path.isdir(dirname):
            os.mkdir(dirname)
            with open(os.path.join(dirname, dirname + '.js')) as bsfile:
                bsfile.write("""/*!
 * Bootstrap.js version @VERSION
 * Copyright (C) 2012, AUTHORS.txt
 * Licensed under the same terms as Bootstrap.js
 */
steal('jquery').then(
""")

